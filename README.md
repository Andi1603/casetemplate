
[defense]: img/cast/defense.png "Phoenix Wright"
[prosecutor]: img/cast/prosecution.png "Miles Edgeworth"
[victim]: img/cast/victim.png "Cindy Stone"
[defendant]: img/cast/defendant.png "Larry Butz"

# Template Turnabout

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

[[_TOC_]]

## Cast
| Defense | Prosecution |
| :------ | :---------- |
| ![alt text][defense] | ![alt text][prosecutor] |
| **Phoenix Wright** | **Miles Edgeworth** |

| Defendant | Victim |
| :-------- | :----- |
| ![alt text][defendant] | ![alt text][victim] |
| **Larry Butz** | **Cindy Stone** |
| **Age:** 25 | **Age:** 22 |
| **Occupation:** Unemployed | **Occupation:** Supermodel |
| Larry is a highly emotional young man, often swinging from sad to happy in a matter of seconds. A friend of Phoenix Wright and defendant in this case. | She lived by herself in an partment and had been dating Larry. |
